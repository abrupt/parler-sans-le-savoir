// Scripts

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// // Prevent bounce effect iOS
// // Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
// var content = document.querySelector('.home');
// content.addEventListener('touchstart', function (event) {
//   this.allowUp = this.scrollTop > 0;
//   this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
//   this.slideBeginY = event.pageY;
// });

// content.addEventListener('touchmove', function (event) {
//   var up = event.pageY > this.slideBeginY;
//   var down = event.pageY < this.slideBeginY;
//   this.slideBeginY = event.pageY;
//   if ((up && this.allowUp) || (down && this.allowDown)) {
//     event.stopPropagation();
//   } else {
//     event.preventDefault();
//   }
// });

// Tools & functions
function randomNb(min, max) { // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
}

// Musique
// var musiqueJoue = "pause";
function musique(track) {
  if (track.paused) {
    track.play();
  } else {
    track.pause();
  }
}

// Menu
const menuBtn = document.querySelector('.btn--menu');
const menu = document.querySelector('.menu');
const musiqueBtn = document.querySelector('.btn--musique');
const hasardBtn = document.querySelector('.btn--hasard');
let menuOpen = false;
let musiquePlay = false;

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menuOpen = !menuOpen;

  const animBtn = document.querySelectorAll('.son-boite');
  if (menuOpen) {
    for (const btn of animBtn) {
      btn.style.zIndex = '90';
    }
  } else {
    for (const btn of animBtn) {
      btn.style.zIndex = '120';
    }
  }
  menu.classList.toggle('show--menu');
  document.querySelector('.btn--menu .info').classList.toggle('info--enable');
  menuBtn.blur();
});

// // Morceaux
// const listeSons = document.querySelector('.liste-sons');
// for (let i = listeSons.children.length; i >= 0; i--) {
//     listeSons.appendChild(listeSons.children[Math.random() * i | 0]);
// }

let morceaux = document.querySelectorAll('audio');
morceaux = Array.from(morceaux);
shuffle(morceaux);

const morceauNb = document.querySelector('.piste__choix');
const morceauNom = document.querySelector('.piste__nom .marquee');
morceauNb.innerHTML = morceaux[0].dataset.num;
morceauNom.innerHTML = morceaux[0].dataset.nom;

musiqueBtn.addEventListener('click', (e) => {
  e.preventDefault();
  musiquePlay = !musiquePlay;
  document.querySelector('.btn--musique .info').classList.toggle('info--enable');
  morceauNom.classList.remove('none');
  musique(morceaux[0]);
  if (!musiquePlay) {
    morceauNom.style.animationPlayState = 'paused';
  } else {
    morceauNom.style.animationPlayState = 'running';
  }
  musiqueBtn.blur();
});

hasardBtn.addEventListener('click', (e) => {
  e.preventDefault();
  if (musiquePlay) {
    musiquePlay = false;
    morceaux[0].pause();
    morceauNom.style.animationPlayState = 'paused';
    document.querySelector('.btn--musique .info').classList.toggle('info--enable');
  }
  shuffle(morceaux);
  morceauNb.innerHTML = morceaux[0].dataset.num;
  morceauNom.innerHTML = morceaux[0].dataset.nom;
  hasardBtn.blur();
});
