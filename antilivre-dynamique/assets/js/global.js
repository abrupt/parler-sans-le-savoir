// Scripts

// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

// Prevent bounce effect iOS
// Source https://gist.github.com/swannknani/eca799795860cff222f70b8675f8c8d8
var content = document.querySelector('.home');
content.addEventListener('touchstart', function (event) {
  this.allowUp = this.scrollTop > 0;
  this.allowDown = this.scrollTop < this.scrollHeight - this.clientHeight;
  this.slideBeginY = event.pageY;
});

content.addEventListener('touchmove', function (event) {
  var up = event.pageY > this.slideBeginY;
  var down = event.pageY < this.slideBeginY;
  this.slideBeginY = event.pageY;
  if ((up && this.allowUp) || (down && this.allowDown)) {
    event.stopPropagation();
  } else {
    event.preventDefault();
  }
});

// Tools & functions
function randomNb(min, max) { // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
}

// Musique
// var musiqueJoue = "pause";
function musique(track) {
  if (track.paused) {
    track.play();
  } else {
    track.pause();
  }
}

// Menu
const menuBtn = document.querySelector('.btn--menu');
const menu = document.querySelector('.menu');
const fondSonore = document.getElementById('fond-sonore');
const musiqueBtn = document.querySelector('.btn--musique');
let menuOpen = false;

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  menuOpen = !menuOpen;

  const animBtn = document.querySelectorAll('.son-boite');
  if (menuOpen) {
    for (const btn of animBtn) {
      btn.style.zIndex = '90';
    }
  } else {
    for (const btn of animBtn) {
      btn.style.zIndex = '120';
    }
  }
  menu.classList.toggle('show--menu');
  document.querySelector('.btn--menu .info').classList.toggle('info--enable');
});

musiqueBtn.addEventListener('click', (e) => {
  e.preventDefault();
  document.querySelector('.btn--musique .info').classList.toggle('info--enable');
  musique(fondSonore);
  const audios = document.querySelectorAll('audio');
  for (let i = 0; i < audios.length; i++) {
    if (audios[i] != fondSonore) {
      audios[i].pause();
      audios[i].previousElementSibling.classList.remove('son--enable');
    }
  }
});

// Morceaux
const listeSons = document.querySelector('.liste-sons');
for (let i = listeSons.children.length; i >= 0; i--) {
    listeSons.appendChild(listeSons.children[Math.random() * i | 0]);
}

const morceaux = document.querySelectorAll('.son');
for (const morceau of morceaux) {

  // Déplacement
  // const nb1 = randomNb(-50,50);
  // const nb2 = randomNb(-50,50);
  // morceau.style.transform = 'translate(' + nb1 + 'px, ' + nb2 + 'px)';
  // morceau.style.zIndex = randomNb(1,morceaux.length);
  // morceau.setAttribute('data-x', nb1);
  // morceau.setAttribute('data-y', nb2);


  // Son
  const son = morceau.nextElementSibling;

  morceau.addEventListener('click', (e) => {
    e.preventDefault();
    const audios = document.querySelectorAll('audio');
    for (let i = 0; i < audios.length; i++) {
      if (audios[i] != son) {
        audios[i].pause();
        // if (!audios[i].previousElementSibling.classList.contains('son--enable')) {}
        audios[i].previousElementSibling.classList.remove('son--enable');
      }
    }
    musique(son);
    morceau.classList.toggle('son--enable');

    if (!document.querySelector('.btn--musique .info').classList.contains('info--enable')) {
      document.querySelector('.btn--musique .info').classList.toggle('info--enable');
    }
  });

  son.addEventListener('ended', function () {
    son.currentTime = 0;
    morceau.classList.toggle('son--enable');
  });
}

//
// Interact.js
//
let zIndexDrag = 0;

const targetInteract = '.son';
interact(targetInteract).draggable({
  listeners: {
    start: function (event) {
      event.target.parentNode.style.zIndex = parseInt(new Date().getTime() / 10 ** 10) + zIndexDrag;
      zIndexDrag += 1;
      event.target.style.pointerEvents = 'none';
    },
    move: dragMoveListener,
    end: function (event) {
      event.target.style.pointerEvents = '';
    },
  },
  inertia: true,
  modifiers: [
    interact.modifiers.restrictRect({
      restriction: '.home',
      endOnly: true,
    }),
  ],
  autoScroll: false,
});

function dragMoveListener(event) {
  let target = event.target;
  // keep the dragged position in the data-x/data-y attributes
  let x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
  let y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

  // translate the element
  target.style.webkitTransform = target.style.transform = 'translateX(' + x + 'px) translateY(' + y + 'px)';

  // update the posiion attributes
  target.setAttribute('data-x', x);
  target.setAttribute('data-y', y);
}

// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener;

if(navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
  interact(targetInteract).on(['tap'], function (event) {
    const sonTarget = event.currentTarget.nextElementSibling;
    const audios = document.querySelectorAll('audio');
    for (let i = 0; i < audios.length; i++) {
      if (audios[i] != sonTarget) {
        audios[i].pause();
        // if (!audios[i].previousElementSibling.classList.contains('son--enable')) {}
        audios[i].previousElementSibling.classList.remove('son--enable');
      }
    }
    musique(sonTarget);
    event.currentTarget.classList.toggle('son--enable');

    if (!document.querySelector('.btn--musique .info').classList.contains('info--enable')) {
      document.querySelector('.btn--musique .info').classList.toggle('info--enable');
    }
  })
}



