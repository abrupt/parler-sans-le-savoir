---
title: "Bruit"
type: "bruit"
sons:
- nom: "Avocats, eggs, sex"
  src: "../etc/bruit/avocats-eggs-sex.mp3"
  num: 1
- nom: "Chien"
  src: "../etc/bruit/chien.mp3"
  num: 2
- nom: "Épure"
  src: "../etc/bruit/epure.mp3"
  num: 3
- nom: "Érenshé Toub"
  src: "../etc/bruit/erenshe-toub.mp3"
  num: 4
- nom: "Et ta Mémé"
  src: "../etc/bruit/et-ta-meme.mp3"
  num: 5
- nom: "Grand Hôtel Société"
  src: "../etc/bruit/grand-hotel-societe.mp3"
  num: 6
- nom: "L’appendicite nasale"
  src: "../etc/bruit/lappendicite-nasale.mp3"
  num: 7
- nom: "Le triposurface"
  src: "../etc/bruit/le-triposurface.mp3"
  num: 8
- nom: "Mon p’tit puteau"
  src: "../etc/bruit/mon-ptit-puteau.mp3"
  num: 9
- nom: "Shlikon Talakan Karné"
  src: "../etc/bruit/shlikon-talakan-karne.mp3"
  num: 10
- nom: "Vingte moins le quart"
  src: "../etc/bruit/vingte-moins-le-quart.mp3"
  num: 11
pistes: 11
---
