# ~/ABRÜPT/FERNAND FERNANDEZ/PARLER SANS LE SAVOIR/*

La [page de ce livre](https://abrupt.cc/fernand-fernandez/parler-sans-le-savoir/) sur le réseau.

## Sur le livre

Entendre qui que ce soit parler "sans" le Savoir est un idéal. Ça arrive
très peu et de moins en moins : on entend partout partout parler "avec"
le Savoir, son petit savoir à soi ou le Grand. On est cernés par des
Je-sais. Et comme notre lot commun est plutôt d'être des Sais-pas, on se
demande. Qu'est-ce qui se passerait si on n'utilisait plus le Savoir
pour se parler ? Si on s'en servait à propos. Depuis où parlerait-on ?
Depuis le cœur, le ventre, le sexe, la simple parole ? Ce serait la fin
du monde tel qu'il est.

Entrer dans *Parler sans le Savoir* c'est entrer à la fois dans le monde
tel qu'il est et dans le monde tel qu'il pourrait être mais qu'il ne
sera jamais. Un lieu où la "conscience accablée de l'enfer que se font
vivre les humains entre eux", travaillée comme elle l'est dans ce texte,
procure en elle-même et se procure à elle-même une évasion possible.
C'est une expérience unique. Le monde de Fernand Fernandez est une
expérience unique où on entend à la fois ceux qui savent, ceux qui
croient savoir, ceux qui ne savent rien, et ceux qui sont libérés de
cette obsession : les fous les simplets les malades, mais aussi ceux qui
utilisent leur parole tout autrement, parce que leur obsession s'est
tournée ailleurs et qu'ils ont trouvé leur liberté propre dans leur
propre parole. Si on ne les écoute pas, et avec le respect qui leur est
dû, ce n'est pas dommage pour eux c'est dommage pour nous. Et s'il
fallait le dire une fois de plus c'est l'occasion : les notions
d'intelligence et de bêtise sont vraiment à reconsidérer, quand "la
crainte d'avoir l'air bête peut développer une terrifiante bêtise au
cube", et que parmi les plus belles intelligences se trouvent celles de
ceux qui sont privés de parole.

Les différentes formes-objets de *Parler sans le Savoir* proposées ici
auraient pu rester inédites, il faut le dire aussi, parce que
l'existence des voix de Fernandez-homme-orchestre qui sont à l'œuvre au
quotidien depuis des années ne dépend ni de leur publicité ni de leur
publication, tant le désir de FF est autre, tant son œuvre est à l'œuvre
--- et sa force donc. FF dit qu'on peut bien lire PSLS comme on veut, y
compris comme une espèce de théâtre psychique, si on n'oublie pas qu'il
s'agit d'en sortir. Lui-même préfère le nommer "psyence-fiction". Et
quand il dit de ce "travail perpétuel" qu'il lui reconnaît "l'ambition
de se débarrasser des représentations que porte le langage et qui
encombrent" en en "convoquant un maximum pour les identifier", quand il
dit que "parler sans le Savoir ne signifie pas parler en ignorant, même
si évidemment l'ignorance a aussi voix au chapitre, mais rebondir dans
un monde d'hémorragie du langage, un monde où le langage comme vecteur
du sens s'autodétruit dans la profusion et l'équivalence des points de
vue, où le bavardage et le débat sont devenus une seconde nature, la
forme par défaut de la conversation réelle, qui force à se réapproprier
le foisonnement souvent toxique des énoncés dans un espace à soi, un
espace où faire entendre ses propres voix, ce qui permet d'échapper
joyeusement à la noyade", il faudrait aussi ressentir avec lui la charge
de celui qui est à l'affût, et travaille à se rapprocher de ce que
serait "une vie simple" --- une chose très complexe pour qui se méfie du
simplisme.

Ce travail est un géant pacifique qui dévore lui-même tout ce qui se mange aux alentours avant de nous le rendre, et qui se fout complètement du bon ordre d'aujourd'hui, de tous les bords établis, même le hors-bord. Il pourrait aussi bien provenir de 1560 ou de la planète XY21. Sauf qu'il vient d'un artiste vivant aujourd'hui. Et en <a href="https://www.antilivre.org/parler-sans-le-savoir/parler-sans-le-savoir.pdf">lisant</a> *Parler sans le Savoir*, en <a href="https://soundcloud.com/cestabrupt/sets/parler-sans-le-savoir">écoutant</a> FF, en <a href="https://www.fernandfernandezpeinture.org/">regardant</a> ses peintures, on a nous aussi une occasion unique de se demander de quoi on se protège en parlant avec le Savoir ? De quoi a-t-on peur ? En étant les porte-voix du Savoir, ses véhicules, de quoi parle-t-on exactement ?  Qu'est-ce qu'on y gagne ? Et surtout qu'est-ce qu'on perd ? Comment pourrait-on faire autrement ? Qu'est devenue notre capacité au babil ? À la glossolalie ? Au simple chant ? À l'invention ? Et beaucoup, beaucoup d'autres questions qui heureusement ébranlent nos socles, puisque le frère siamois de Savoir ce n'est ni Donner ni Partager, et ça c'est un malheur terrible mais c'est Pouvoir --- ce faux frère qui depuis toujours semble-t-il nous passionne par ici. <em>(clv)</em>

## Sur l'auteur

Dire que Fernand Fernandez est artiste ça devrait suffire sans qu'on ait à cocher les cases poète-peintre-musicien-quoi-encore ? parce que les cases on en a soupé elles servent aux curriculum vitae et aux hashtags c'est certain puis à d'autres mots romains sans doute mais elles nous coincent surtout dans l'illusion ou le désir d'un monde bien rangé et bien contrôlable alors que rien n'est rangé et que rien n'est contrôlable on devrait le savoir depuis le temps. Pour l'incasable cas Fernand Fernandez voilà les cases à cocher : il est inhashtagable inrangeable incontrôlable et inspécialisable. Son curriculum vitae c'est le déroulement de sa vie voilà. C'est être, c'est être artiste, ça pourrait être être-artiste. En tout cas s'engouffrer dans les cases grandes ouvertes que sont <a href="https://abrupt.cc/fernand-fernandez/parler-sans-le-savoir/">lire</a> et <a href="https://www.fernandfernandezpeinture.org/">regarder</a> et <a href="https://soundcloud.com/cestabrupt/sets/parler-sans-le-savoir">écouter</a>, ça ça devrait amplement suffire. <em>(clv)</em>

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
